const mongoose = require('mongoose')
const Building = require('./models/Building')
const Room = require('./models/Room')
mongoose.connect('mongodb://localhost:27017/example')

async function main () {
  // Update
  // const room = await Room.findById('6218f94b96669186047b0060')
  // room.capacity = 20
  // room.save()
  // console.log(room)

  // const room = await Room.findOne({ _id: '6218f94b96669186047b0060' })
  // console.log(room)

  // const rooms = await Room.find({ capacity: { $gte: 100 } })
  // console.log(rooms)

  const room = await Room.findOne({ capacity: { $gte: 100 } }).populate('building')
  console.log(room)
  console.log('---------------------------------------------')
  const rooms = await Room.find({ capacity: { $gte: 100 } }).populate('building')
  console.log(rooms)
  console.log('---------------------------------------------')
  const buildings = await Building.find({}).populate('rooms')
  console.log(JSON.stringify(buildings))
}

main().then(function () {
  console.log('Finish')
})
